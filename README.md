Usage
===
```
python3 autotest.py -t cpp yourcode.cpp input.txt

# On another terminal
vim input.txt
vim yourcode.cpp
```

Sample input.txt
---
```
type: plain

10 20
---

30 40
---
type: exec

print(2**30, 2**30)
```

Sample yourcode.cpp
---
```
#include <iostream>
int main() {
    int x, y;
    std::cin >> x >> y;
    std::cout << x + y << std::endl;
}
```

Dependencies
===
* pyinotify
