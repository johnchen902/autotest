#!/usr/bin/env python3
import argparse
import os
import resource
import shutil
import signal
import subprocess
import sys

import pyinotify

def safe_run(*popenargs, input=None, timeout=None, check=False, **kwargs):
    """subprocess.run() but SIGINT and SIGQUIT only kill the child"""
    if input is not None:
        if 'stdin' in kwargs:
            raise ValueError('stdin and input arguments may not both be used.')
        kwargs['stdin'] = subprocess.PIPE

    with subprocess.Popen(*popenargs, **kwargs) as process:
        int_signal = signal.signal(signal.SIGINT, signal.SIG_IGN)
        quit_signal = signal.signal(signal.SIGQUIT, signal.SIG_IGN)
        try:
            stdout, stderr = process.communicate(input, timeout=timeout)
        except subprocess.TimeoutExpired:
            process.kill()
            stdout, stderr = process.communicate()
            raise subprocess.TimeoutExpired(process.args, timeout,
                                            output=stdout, stderr=stderr)
        except:
            process.kill()
            process.wait()
            raise
        finally:
            signal.signal(signal.SIGINT, int_signal)
            signal.signal(signal.SIGQUIT, quit_signal)
        retcode = process.poll()
        if check and retcode:
            raise subprocess.CalledProcessError(retcode, process.args,
                                                output=stdout, stderr=stderr)
    return subprocess.CompletedProcess(process.args, retcode, stdout, stderr)

def get_entry(stream):
    """Get one entry from stream.

    The format of entry is:
    1. Multiple '<key>: <value>' lines
    2. An empty line
    3. Multiple content lines
    4. A line '---', or EOF
    """
    options = {}
    for line in stream:
        if line == '\n':
            break
        try:
            key, value = line.split(':', 1)
        except ValueError:
            raise ValueError("Header does not match format: '<key>: <value>'")
        options[key.strip()] = value.strip()
    else:
        raise EOFError

    contents = []
    for line in stream:
        if line == '---\n':
            break
        contents.append(line)

    return ''.join(contents), options

def entry_to_input(entry):
    """Convert an entry to actual input

    If options['type'] == 'exec', the content is exec()-ed
    and print()-ed data are used as real content.
    """
    content, options = entry

    content_type = options.pop('type', 'plain')
    if content_type == 'exec':
        newcontents = []
        def myprint(*values, sep=' ', end='\n'):
            newcontents.append(sep.join(map(str, values)) + end)
        exec(content, {'print': myprint})
        content = ''.join(newcontents)

    return content.encode('utf-8'), options

def get_inputs(stream):
    """Parse all input entries in a stream and produce corresponding inputs"""
    inputs = []
    while True:
        try:
            entry = get_entry(stream)
        except EOFError:
            break
        inputs.append(entry_to_input(entry))
    return inputs

def samepath(path1, path2):
    return os.path.abspath(path1) == os.path.abspath(path2)

def print_banner(x):
    width, _ = shutil.get_terminal_size()
    print((' ' + x + ' ').center(width, '\u2550'))

class Tester:
    def __init__(self):
        self._tests = {}
        self._runtests = {}
        self._commands = {}

    def update_input(self, name, stream):
        print_banner("Updating input")
        try:
            self._tests[name] = get_inputs(stream)
        except Exception as e:
            print(e, file=sys.stderr)
            self._tests.pop(name, None)
        else:
            self.test(name)

    def set_command(self, name, command):
        self._commands[name] = command
        self._runtests.pop(name, None)
        self.test(name)

    def test(self, name):
        try:
            tests = self._tests[name]
            runtests = self._runtests.setdefault(name, set())
            command = self._commands[name]
        except KeyError:
            return

        for i, (test, options) in enumerate(tests):
            if test in runtests:
                continue
            runtests.add(test)

            print_banner("Running test %d" % i)

            extra_args = {'input': test}
            if 'nooutput' in options:
                extra_args['stdout'] = subprocess.DEVNULL
                print_banner("(output suppressed)")

            r1 = resource.getrusage(resource.RUSAGE_CHILDREN)
            proc = safe_run(command, **extra_args)
            r2 = resource.getrusage(resource.RUSAGE_CHILDREN)

            if proc.returncode != 0:
                if proc.returncode > 0:
                    print_banner("Exited with status %d" % proc.returncode)
                else:
                    print_banner("Terminated by signal %d" % -proc.returncode)
                break

            ru_utime = r2.ru_utime - r1.ru_utime
            ru_stime = r2.ru_stime - r1.ru_stime
            print_banner("User %f System %f" % (ru_utime, ru_stime))

class MyEventHandler(pyinotify.ProcessEvent):
    def __init__(self):
        super().__init__()

        self._tester = Tester()

    def process_default(self, event):
        path = event.pathname
        name, ext = os.path.splitext(path)

        if os.access(path, os.X_OK):
            self._tester.set_command(name, [path])
        elif ext == '.txt':
            try:
                stream = open(path)
            except OSError:
                pass
            else:
                self._tester.update_input(name, stream)

def main():
    parser = argparse.ArgumentParser("autotest")
    parser.add_argument("dir", nargs='?', default=".",
            help="monitor DIR for changes")
    args = parser.parse_args()

    wm = pyinotify.WatchManager()
    handler = MyEventHandler()
    notifier = pyinotify.Notifier(wm, handler)

    mask = pyinotify.IN_CLOSE_WRITE
    wm.add_watch(args.dir, mask)

    notifier.loop()

if __name__ == '__main__':
    sys.exit(main())
